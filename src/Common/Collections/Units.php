<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 02.02.2020
 * Time: 19:50
 */

declare(strict_types=1);

namespace App\Common\Collections;

use App\Model\UnitInterface;

/**
 * Class Units
 *
 * @package App\Common\Collections
 */
final class Units extends Collection
{
    /**
     * Units constructor.
     *
     * @param array $elements
     */
    public function __construct(array $elements = [])
    {
        parent::__construct(UnitInterface::class, $elements);
    }

    /**
     * @param array $elements
     *
     * @return Units
     */
    protected function createFrom(array $elements)
    {
        return new self($elements);
    }
}