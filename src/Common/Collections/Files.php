<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 02.02.2020
 * Time: 19:50
 */

declare(strict_types=1);

namespace App\Common\Collections;

use Symfony\Component\Finder\SplFileInfo;

/**
 * Class Files
 *
 * @package App\Common\Collections
 */
final class Files extends Collection
{
    /**
     * Files constructor.
     *
     * @param array $elements
     */
    public function __construct(array $elements = [])
    {
        parent::__construct(SplFileInfo::class, $elements);
    }

    /**
     * @param array $elements
     *
     * @return Files
     */
    protected function createFrom(array $elements)
    {
        return new self($elements);
    }
}