<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 02.02.2020
 * Time: 23:25
 */

declare(strict_types=1);

namespace App\Common\Collections;

use Doctrine\Common\Collections\ArrayCollection;
use LogicException;

/**
 * Class Collection
 *
 * @package App\Common\Collections
 */
abstract class Collection extends ArrayCollection
{
    private string $type;

    /**
     * Collection constructor.
     *
     * @param string $type
     * @param array $elements
     */
    public function __construct(string $type, array $elements = [])
    {
        if (empty($type)) {
            throw new LogicException('The $type variable cannot be empty.');
        }

        $this->type = $type;
        parent::__construct($this->filterExpected($elements));
    }

    /**
     * @param mixed $element
     *
     * @return bool
     */
    private function isExpectedType($element): bool
    {
        return $element instanceof $this->type;
    }

    /**
     * @param array $elements
     *
     * @return array
     */
    private function filterExpected(array $elements): array
    {
        return array_values(
            array_filter(
                $elements,
                function ($element) {
                    return $this->isExpectedType($element);
                }
            )
        );
    }

    /**
     * @param mixed $element
     *
     * @return true
     */
    public function add($element)
    {
        if (!$this->isExpectedType($element)) {
            return true;
        }

        return parent::add($element);
    }

    public function set($key, $value)
    {
        if (!$this->isExpectedType($value)) {
            return;
        }

        parent::set($key, $value);
    }

    public function removeElement($element)
    {
        if (!$this->isExpectedType($element)) {
            return false;
        }

        return parent::removeElement($element);
    }
}