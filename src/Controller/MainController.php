<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 28.01.2020
 * Time: 21:52
 */

declare(strict_types=1);

namespace App\Controller;

use App\Service\CryptoInterface;
use App\Service\Manager\World\WorldManager;
use App\Service\Manager\World\WorldsManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class MainController
 *
 * @package App\Controller
 */
final class MainController extends AbstractController
{
    private CryptoInterface $crypto;
    private ?string $locale;
    private SerializerInterface $serializer;
    private SessionInterface $session;
    private SluggerInterface $slugger;
    private TranslatorInterface $translator;
    private WorldManager $worldManager;
    private WorldsManager $worldsManager;

    /**
     * MainController constructor.
     *
     * @param ContainerInterface $container
     * @param CryptoInterface $crypto
     * @param RequestStack $requestStack
     * @param SerializerInterface $serializer
     * @param SessionInterface $session
     * @param SluggerInterface $slugger
     * @param TranslatorInterface $translator
     * @param WorldManager $worldManager
     * @param WorldsManager $worldsManager
     */
    public function __construct(
        ContainerInterface $container,
        CryptoInterface $crypto,
        RequestStack $requestStack,
        SerializerInterface $serializer,
        SessionInterface $session,
        SluggerInterface $slugger,
        TranslatorInterface $translator,
        WorldManager $worldManager,
        WorldsManager $worldsManager
    ) {
        $this->container = $container;
        $this->crypto = $crypto;
        $this->locale = null;
        $this->serializer = $serializer;
        $this->session = $session;
        $this->slugger = $slugger;
        $this->translator = $translator;
        $this->worldManager = $worldManager;
        $this->worldsManager = $worldsManager;

        $request = $requestStack->getCurrentRequest();

        if ($request !== null) {
            $this->locale = $request->getLocale();
        }
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/", methods={"GET"})
     * @Route("/worlds", name="homepage", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        return $this->render(
            'base.html.twig',
            [
                'opened' => $this->serializer->serialize(
                    $this->worldsManager->getOpened(),
                    'json'
                ),
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $worldId
     *
     * @return RedirectResponse|Response
     *
     * @Route("/worlds/{worldId}", name="calculate.distance.action", methods={"GET", "POST"})
     */
    public function calculatorAction(Request $request, string $worldId): Response
    {
        $world = $this->worldsManager->getOpenedWorldById($worldId);

        if ($world === null) {
            // todo: needs add flash message
            return $this->redirectToRoute('homepage');
        }

        $this->worldManager->setWorld($world);
        $this->worldManager->setFilesDirectory();
        $this->worldManager->fillConfig();
        $this->worldManager->fillUnits();

        return $this->render(
            'base.html.twig',
            [
                'world' => $this->serializer->serialize(
                    $world,
                    'json'
                ),
            ]
        );
    }
}
