<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 07.02.2020
 * Time: 19:52
 */

declare(strict_types=1);

namespace App\Model\Api\World;

use App\Model\Api\World\Config\Commands;
use App\Model\Api\World\Config\Game;

/**
 * Class Config
 *
 * @package App\Model\Api\World
 */
final class Config
{
    private string $speed;
    private string $unitSpeed;
    private Commands $commands;
    private Game $game;

    /**
     * Config constructor.
     *
     * @param string $speed
     * @param string $unitSpeed
     * @param Commands $commands
     * @param Game $game
     */
    public function __construct(string $speed, string $unitSpeed, Commands $commands, Game $game)
    {
        $this->speed = $speed;
        $this->unitSpeed = $unitSpeed;
        $this->commands = $commands;
        $this->game = $game;
    }

    /**
     * @return string
     */
    public function getSpeed(): string
    {
        return $this->speed;
    }

    /**
     * @return string
     */
    public function getUnitSpeed(): string
    {
        return $this->unitSpeed;
    }

    /**
     * @return Commands
     */
    public function getCommands(): Commands
    {
        return $this->commands;
    }

    /**
     * @return Game
     */
    public function getGame(): Game
    {
        return $this->game;
    }
}