<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 10.02.2020
 * Time: 23:32
 */

declare(strict_types=1);

namespace App\Model\Api\World\Config;

/**
 * Class Commands
 *
 * @package App\Model\Api\World\Config
 */
final class Commands
{
    private string $millisArrival;

    /**
     * Commands constructor.
     *
     * @param string $millisArrival
     */
    public function __construct(string $millisArrival)
    {
        $this->millisArrival = $millisArrival;
    }

    /**
     * @return string
     */
    public function getMillisArrival(): string
    {
        return $this->millisArrival;
    }
}