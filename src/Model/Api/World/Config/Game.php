<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 07.02.2020
 * Time: 19:42
 */

declare(strict_types=1);

namespace App\Model\Api\World\Config;

/**
 * Class Game
 *
 * @package App\Model\Api\World\Config
 */
final class Game
{
    private string $archer;
    private string $knight;

    /**
     * Game constructor.
     *
     * @param string $archer
     * @param string $knight
     */
    public function __construct(string $archer, string $knight)
    {
        $this->archer = $archer;
        $this->knight = $knight;
    }

    /**
     * @return string
     */
    public function getArcher(): string
    {
        return $this->archer;
    }

    /**
     * @return string
     */
    public function getKnight(): string
    {
        return $this->knight;
    }
}