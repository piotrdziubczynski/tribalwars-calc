<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 13.02.2020
 * Time: 20:53
 */

declare(strict_types=1);

namespace App\Model\Api\Units\Config;

/**
 * Class AbstractUnit
 *
 * @package App\Model\Api\Units\Config
 */
abstract class AbstractUnit
{
    protected string $name;
    protected string $speed;
    protected string $attack;
    protected string $defense;
    protected string $defenseCavalry;
    protected string $defenseArcher;

    /**
     * AbstractUnit constructor.
     *
     * @param string $name
     * @param string $speed
     * @param string $attack
     * @param string $defense
     * @param string $defenseCavalry
     * @param string $defenseArcher
     */
    public function __construct(
        string $name,
        string $speed,
        string $attack,
        string $defense,
        string $defenseCavalry,
        string $defenseArcher
    ) {
        $this->name = $name;
        $this->speed = $speed;
        $this->attack = $attack;
        $this->defense = $defense;
        $this->defenseCavalry = $defenseCavalry;
        $this->defenseArcher = $defenseArcher;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSpeed(): string
    {
        return $this->speed;
    }

    /**
     * @return string
     */
    public function getAttack(): string
    {
        return $this->attack;
    }

    /**
     * @return string
     */
    public function getDefense(): string
    {
        return $this->defense;
    }

    /**
     * @return string
     */
    public function getDefenseCavalry(): string
    {
        return $this->defenseCavalry;
    }

    /**
     * @return string
     */
    public function getDefenseArcher(): string
    {
        return $this->defenseArcher;
    }
}