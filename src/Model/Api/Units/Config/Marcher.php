<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 13.02.2020
 * Time: 21:02
 */

declare(strict_types=1);

namespace App\Model\Api\Units\Config;

/**
 * Class Marcher
 *
 * @package App\Model\Api\Units\Config
 */
final class Marcher extends AbstractUnit
{
    /**
     * Marcher constructor.
     *
     * @param string $attack
     * @param string $defense
     * @param string $defenseCavalry
     * @param string $defenseArcher
     */
    public function __construct(
        string $attack,
        string $defense,
        string $defenseCavalry,
        string $defenseArcher
    ) {
        parent::__construct(
            'marcher',
            '600',
            $attack,
            $defense,
            $defenseCavalry,
            $defenseArcher
        );
    }
}