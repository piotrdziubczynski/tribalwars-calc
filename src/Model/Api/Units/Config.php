<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 12.02.2020
 * Time: 23:14
 */

declare(strict_types=1);

namespace App\Model\Api\Units;

use App\Model\Api\Units\Config\Archer;
use App\Model\Api\Units\Config\Axe;
use App\Model\Api\Units\Config\Catapult;
use App\Model\Api\Units\Config\Heavy;
use App\Model\Api\Units\Config\Knight;
use App\Model\Api\Units\Config\Light;
use App\Model\Api\Units\Config\Marcher;
use App\Model\Api\Units\Config\Militia;
use App\Model\Api\Units\Config\Ram;
use App\Model\Api\Units\Config\Snob;
use App\Model\Api\Units\Config\Spear;
use App\Model\Api\Units\Config\Spy;
use App\Model\Api\Units\Config\Sword;

/**
 * Class Config
 *
 * @package App\Model\Api\Units
 */
final class Config
{
    private ?Spear $spear;
    private ?Sword $sword;
    private ?Axe $axe;
    private ?Archer $archer;
    private ?Spy $spy;
    private ?Light $light;
    private ?Marcher $marcher;
    private ?Heavy $heavy;
    private ?Ram $ram;
    private ?Catapult $catapult;
    private ?Knight $knight;
    private ?Snob $snob;
    private ?Militia $militia;

    /**
     * Config constructor.
     *
     * @param Spear|null $spear
     * @param Sword|null $sword
     * @param Axe|null $axe
     * @param Archer|null $archer
     * @param Spy|null $spy
     * @param Light|null $light
     * @param Marcher|null $marcher
     * @param Heavy|null $heavy
     * @param Ram|null $ram
     * @param Catapult|null $catapult
     * @param Knight|null $knight
     * @param Snob|null $snob
     * @param Militia|null $militia
     */
    public function __construct(
        ?Spear $spear = null,
        ?Sword $sword = null,
        ?Axe $axe = null,
        ?Archer $archer = null,
        ?Spy $spy = null,
        ?Light $light = null,
        ?Marcher $marcher = null,
        ?Heavy $heavy = null,
        ?Ram $ram = null,
        ?Catapult $catapult = null,
        ?Knight $knight = null,
        ?Snob $snob = null,
        ?Militia $militia = null
    )
    {
        $this->spear = $spear;
        $this->sword = $sword;
        $this->axe = $axe;
        $this->archer = $archer;
        $this->spy = $spy;
        $this->light = $light;
        $this->marcher = $marcher;
        $this->heavy = $heavy;
        $this->ram = $ram;
        $this->catapult = $catapult;
        $this->knight = $knight;
        $this->snob = $snob;
        $this->militia = $militia;
    }

    /**
     * @return Spear|null
     */
    public function getSpear(): ?Spear
    {
        return $this->spear;
    }

    /**
     * @return Sword|null
     */
    public function getSword(): ?Sword
    {
        return $this->sword;
    }

    /**
     * @return Axe|null
     */
    public function getAxe(): ?Axe
    {
        return $this->axe;
    }

    /**
     * @return Archer|null
     */
    public function getArcher(): ?Archer
    {
        return $this->archer;
    }

    /**
     * @return Spy|null
     */
    public function getSpy(): ?Spy
    {
        return $this->spy;
    }

    /**
     * @return Light|null
     */
    public function getLight(): ?Light
    {
        return $this->light;
    }

    /**
     * @return Marcher|null
     */
    public function getMarcher(): ?Marcher
    {
        return $this->marcher;
    }

    /**
     * @return Heavy|null
     */
    public function getHeavy(): ?Heavy
    {
        return $this->heavy;
    }

    /**
     * @return Ram|null
     */
    public function getRam(): ?Ram
    {
        return $this->ram;
    }

    /**
     * @return Catapult|null
     */
    public function getCatapult(): ?Catapult
    {
        return $this->catapult;
    }

    /**
     * @return Knight|null
     */
    public function getKnight(): ?Knight
    {
        return $this->knight;
    }

    /**
     * @return Snob|null
     */
    public function getSnob(): ?Snob
    {
        return $this->snob;
    }

    /**
     * @return Militia|null
     */
    public function getMilitia(): ?Militia
    {
        return $this->militia;
    }
}