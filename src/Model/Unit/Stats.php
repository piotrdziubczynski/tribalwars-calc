<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 13.02.2020
 * Time: 00:07
 */

declare(strict_types=1);

namespace App\Model\Unit;

use App\Model\Unit\Stats\Attack;
use App\Model\Unit\Stats\Defense;

/**
 * Class Stats
 *
 * @package App\Model\Unit
 */
final class Stats
{
    private Attack $attack;
    private Defense $defense;

    /**
     * Stats constructor.
     *
     * @param Attack $attack
     * @param Defense $defense
     */
    public function __construct(Attack $attack, Defense $defense)
    {
        $this->attack = $attack;
        $this->defense = $defense;
    }

    /**
     * @return Attack
     */
    public function getAttack(): Attack
    {
        return $this->attack;
    }

    /**
     * @return Defense
     */
    public function getDefense(): Defense
    {
        return $this->defense;
    }
}