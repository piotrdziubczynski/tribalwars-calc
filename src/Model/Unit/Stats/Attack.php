<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 12.02.2020
 * Time: 23:47
 */

declare(strict_types=1);

namespace App\Model\Unit\Stats;

/**
 * Class Attack
 *
 * @package App\Model\Unit\Stats
 */
final class Attack
{
    private int $general;

    /**
     * Attack constructor.
     *
     * @param string $general
     */
    public function __construct(string $general)
    {
        $this->general = (int)$general;
    }

    /**
     * @return int
     */
    public function getGeneral(): int
    {
        return $this->general;
    }
}