<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 12.02.2020
 * Time: 23:47
 */

declare(strict_types=1);

namespace App\Model\Unit\Stats;

/**
 * Class Defense
 *
 * @package App\Model\Unit\Stats
 */
final class Defense
{
    private int $general;
    private int $cavalry;
    private int $archer;

    /**
     * Defense constructor.
     *
     * @param string $general
     * @param string $cavalry
     * @param string $archer
     */
    public function __construct(string $general, string $cavalry, string $archer)
    {
        $this->general = (int)$general;
        $this->cavalry = (int)$cavalry;
        $this->archer = (int)$archer;
    }

    /**
     * @return int
     */
    public function getGeneral(): int
    {
        return $this->general;
    }

    /**
     * @return int
     */
    public function getCavalry(): int
    {
        return $this->cavalry;
    }

    /**
     * @return int
     */
    public function getArcher(): int
    {
        return $this->archer;
    }
}