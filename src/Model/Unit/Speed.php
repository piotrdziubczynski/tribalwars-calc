<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 17.02.2020
 * Time: 23:54
 */

declare(strict_types=1);

namespace App\Model\Unit;

/**
 * Class Speed
 *
 * @package App\Model\Unit
 */
final class Speed
{
    private float $seconds;
    private float $minutes;
    private float $hours;

    /**
     * Speed constructor.
     *
     * @param float $seconds
     */
    public function __construct(float $seconds)
    {
        $this->seconds = round($seconds, 12) ?: 0.0;
        $this->minutes = round($this->seconds / 60, 12) ?: 0.0;
        $this->hours = round($this->minutes / 60, 12) ?: 0.0;
    }

    /**
     * @return float
     */
    public function getSeconds(): float
    {
        return $this->seconds;
    }

    /**
     * @return float
     */
    public function getMinutes(): float
    {
        return $this->minutes;
    }

    /**
     * @return float
     */
    public function getHours(): float
    {
        return $this->hours;
    }
}