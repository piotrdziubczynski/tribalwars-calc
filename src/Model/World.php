<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 02.02.2020
 * Time: 18:08
 */

declare(strict_types=1);

namespace App\Model;

use App\Common\Collections\Units;
use App\Model\World\Config;
use App\Model\World\Data;

/**
 * Class World
 *
 * @package App\Model
 */
final class World implements WorldInterface
{
    public const OPENED = 'opened';
    public const CLOSED = 'closed';

    private string $id;
    private string $name;
    private Data $data;
    private ?Config $config;
    private ?Units $units;

    /**
     * World constructor.
     *
     * @param string $id
     * @param string $name
     * @param Data $data
     */
    public function __construct(
        string $id,
        string $name,
        Data $data
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->data = $data;
        $this->config = null;
        $this->units = null;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Data
     */
    public function getData(): Data
    {
        return $this->data;
    }

    /**
     * @param Data $data
     */
    public function setData(Data $data): void
    {
        $this->data = $data;
    }

    /**
     * @return Config|null
     */
    public function getConfig(): ?Config
    {
        return $this->config;
    }

    /**
     * @param Config|null $config
     */
    public function setConfig(?Config $config): void
    {
        $this->config = $config;
    }

    /**
     * @return Units|null
     */
    public function getUnits(): ?Units
    {
        return $this->units;
    }

    /**
     * @param Units|null $units
     */
    public function setUnits(?Units $units): void
    {
        $this->units = $units;
    }
}