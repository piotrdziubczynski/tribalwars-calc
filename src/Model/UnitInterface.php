<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 12.02.2020
 * Time: 23:26
 */

declare(strict_types=1);

namespace App\Model;

use App\Model\Unit\Speed;
use App\Model\Unit\Stats;

/**
 * Interface UnitInterface
 *
 * @package App\Model
 */
interface UnitInterface
{
    public function getName(): string;
    public function getSpeed(): Speed;
    public function getStats(): Stats;
}