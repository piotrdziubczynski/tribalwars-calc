<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 12.02.2020
 * Time: 23:41
 */

declare(strict_types=1);

namespace App\Model;

use App\Model\Unit\Speed;
use App\Model\Unit\Stats;

/**
 * Class Unit
 *
 * @package App\Model
 */
final class Unit implements UnitInterface
{
    private string $name;
    private Speed $speed;
    private Stats $stats;

    /**
     * Unit constructor.
     *
     * @param string $name
     * @param Speed $speed
     * @param Stats $stats
     */
    public function __construct(string $name, Speed $speed, Stats $stats)
    {
        $this->name = mb_strtolower($name);
        $this->speed = $speed;
        $this->stats = $stats;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Speed
     */
    public function getSpeed(): Speed
    {
        return $this->speed;
    }

    /**
     * @return Stats
     */
    public function getStats(): Stats
    {
        return $this->stats;
    }
}