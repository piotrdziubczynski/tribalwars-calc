<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 02.02.2020
 * Time: 18:12
 */

declare(strict_types=1);

namespace App\Model\World;

/**
 * Class Data
 *
 * @package App\Model\World
 */
final class Data
{
    private int $players;
    private int $tribes;
    private int $villages;

    /**
     * Data constructor.
     *
     * @param int $players
     * @param int $tribes
     * @param int $villages
     */
    public function __construct(int $players, int $tribes, int $villages)
    {
        $this->players = $players;
        $this->tribes = $tribes;
        $this->villages = $villages;
    }

    /**
     * @return int
     */
    public function getPlayers(): int
    {
        return $this->players;
    }

    /**
     * @return int
     */
    public function getTribes(): int
    {
        return $this->tribes;
    }

    /**
     * @return int
     */
    public function getVillages(): int
    {
        return $this->villages;
    }
}