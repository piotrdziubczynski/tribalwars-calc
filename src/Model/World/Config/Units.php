<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 03.02.2020
 * Time: 23:36
 */

declare(strict_types=1);

namespace App\Model\World\Config;

/**
 * Class Units
 *
 * @package App\Model\World\Config
 */
final class Units
{
    private bool $archer;
    private bool $knight;

    /**
     * Units constructor.
     *
     * @param string $archer
     * @param string $knight
     */
    public function __construct(string $archer, string $knight)
    {
        $this->archer = (bool)(int)$archer;
        $this->knight = (bool)(int)$knight;
    }

    /**
     * @return bool
     */
    public function isArcher(): bool
    {
        return $this->archer;
    }

    /**
     * @return bool
     */
    public function isKnight(): bool
    {
        return $this->knight;
    }
}