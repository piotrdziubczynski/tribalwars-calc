<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 02.02.2020
 * Time: 18:36
 */

declare(strict_types=1);

namespace App\Model\World\Config;

/**
 * Class Speed
 *
 * @package App\Model\World\Config
 */
final class Speed
{
    private float $game;
    private float $units;
    private float $world;

    /**
     * Speed constructor.
     *
     * @param string $game
     * @param string $units
     */
    public function __construct(string $game, string $units)
    {
        $this->game = round((float)$game, 12) ?: 0.0;
        $this->units = round((float)$units, 12) ?: 0.0;
        $this->world = round($this->game * $this->units, 12) ?: 0.0;
    }

    /**
     * @return float
     */
    public function getGame(): float
    {
        return $this->game;
    }

    /**
     * @return float
     */
    public function getUnits(): float
    {
        return $this->units;
    }

    /**
     * @return float
     */
    public function getWorld(): float
    {
        return $this->world;
    }
}