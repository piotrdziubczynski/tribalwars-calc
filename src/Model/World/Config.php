<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 02.02.2020
 * Time: 18:35
 */

declare(strict_types=1);

namespace App\Model\World;

use App\Model\World\Config\Speed;
use App\Model\World\Config\Units;

/**
 * Class Config
 *
 * @package App\Model\World
 */
final class Config
{
    private bool $milliseconds;
    private Speed $speed;
    private Units $units;

    /**
     * Config constructor.
     *
     * @param string $milliseconds
     * @param Speed $speed
     * @param Units $units
     */
    public function __construct(string $milliseconds, Speed $speed, Units $units)
    {
        $this->milliseconds = (bool)(int)$milliseconds;
        $this->speed = $speed;
        $this->units = $units;
    }

    /**
     * @return bool
     */
    public function isMilliseconds(): bool
    {
        return $this->milliseconds;
    }

    /**
     * @return Speed
     */
    public function getSpeed(): Speed
    {
        return $this->speed;
    }

    /**
     * @return Units
     */
    public function getUnits(): Units
    {
        return $this->units;
    }
}