<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 02.02.2020
 * Time: 20:04
 */

declare(strict_types=1);

namespace App\Model;

use App\Common\Collections\Units;
use App\Model\World\Config;
use App\Model\World\Data;

/**
 * Interface WorldInterface
 *
 * @package App\Model
 */
interface WorldInterface
{
    public function getId(): string;
    public function setId(string $slug): void;
    public function getName(): string;
    public function setName(string $name): void;
    public function getData(): Data;
    public function setData(Data $data): void;
    public function getConfig(): ?Config;
    public function setConfig(?Config $config): void;
    public function getUnits(): ?Units;
    public function setUnits(?Units $units): void;
}