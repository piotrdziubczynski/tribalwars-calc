<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 06.02.2020
 * Time: 21:44
 */

declare(strict_types=1);

namespace App\Service;

/**
 * Interface CryptoInterface
 *
 * @package App\Service
 */
interface CryptoInterface
{
    /**
     * @param mixed $data
     * @param array $context
     *
     * @return string
     */
    public function encode($data, array $context = []): string;

    /**
     * @param string $data
     * @param string $type
     * @param array $context
     *
     * @return mixed
     */
    public function decode(string $data, string $type, array $context = []);
}