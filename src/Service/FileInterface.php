<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 10.02.2020
 * Time: 23:11
 */

declare(strict_types=1);

namespace App\Service;

use App\Common\Collections\Files;

/**
 * Interface FileInterface
 *
 * @package App\Service
 */
interface FileInterface
{
    /**
     * @param string $directory
     */
    public function setDirectory(string $directory = ''): void;

    /**
     * @param string $filename
     *
     * @return bool
     */
    public function createTxtFile(string $filename): bool;

    /**
     * @param string $filename
     * @param string $content
     *
     * @return bool
     */
    public function saveToTxtFile(string $filename, string $content): bool;

    /**
     * @param string $filename
     *
     * @return Files|null
     */
    public function getTxtFiles(string $filename): ?Files;

    public function reset(): void;
}