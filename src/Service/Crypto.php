<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 06.02.2020
 * Time: 21:27
 */

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class CryptoService
 *
 * @package App\Service
 */
final class Crypto implements CryptoInterface
{
    private SerializerInterface $serializer;

    /**
     * Crypto constructor.
     *
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @inheritDoc
     */
    public function encode($data, array $context = []): string
    {
        return base64_encode($this->serializer->serialize($data, 'json', $context));
    }

    /**
     * @inheritDoc
     */
    public function decode(string $data, string $type, array $context = [])
    {
        $data = base64_decode($data, true) ?: '';

        if (empty($type)) {
            return json_decode($data, true);
        }

        return $this->serializer->deserialize($data, $type, 'json', $context);
    }
}