<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 28.01.2020
 * Time: 21:51
 */

declare(strict_types=1);

namespace App\Service;

use Goutte\Client;
use RuntimeException;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Fetch
 *
 * @package App\Service
 */
final class Fetch implements FetchInterface
{
    private Client $client;
    private ?Crawler $crawler;

    /**
     * Fetch constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
        $this->crawler = null;
    }

    /**
     * @inheritDoc
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @inheritDoc
     */
    public function getDom(): Crawler
    {
        if ($this->crawler === null) {
            throw new RuntimeException('Before using this method, make the request first.');
        }

        return $this->crawler;
    }

    /**
     * @inheritDoc
     */
    public function get(string $uri, array $parameters = []): void
    {
        $this->crawler = $this->client->request('GET', $uri, $parameters);
    }

    /**
     * @inheritDoc
     */
    public function post(string $uri, array $parameters = []): void
    {
        $this->crawler = $this->client->request('POST', $uri, $parameters);
    }
}