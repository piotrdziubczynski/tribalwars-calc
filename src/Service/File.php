<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 10.02.2020
 * Time: 23:12
 */

declare(strict_types=1);

namespace App\Service;

use App\Common\Collections\Files;
use LogicException;
use RuntimeException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Throwable;

/**
 * Class File
 *
 * @package App\Service
 */
final class File implements FileInterface
{
    private ?string $directory;
    private ?Files $files;
    private string $filesDir;

    /**
     * File constructor.
     *
     * https://symfony.com/doc/current/components/filesystem.html
     * https://symfony.com/doc/current/components/finder.html
     *
     * @param string $projectDir
     */
    public function __construct(string $projectDir)
    {
        $this->directory = null;
        $this->files = null;
        $this->filesDir = sprintf('%s/var/files', $projectDir);
    }

    /**
     * @param string $filename
     * @param string $extension
     * @param string $content
     *
     * @return bool
     */
    private function saveToFile(string $filename, string $extension, string $content = ''): bool
    {
        if ($this->directory === null) {
            throw new RuntimeException('Before using this method, set the directory first.');
        }

        if (empty($filename)) {
            throw new LogicException('The filename cannot be empty.');
        }

        if (empty($extension)) {
            throw new LogicException('The extension cannot be empty.');
        }

        $filesystem = new Filesystem();

        try {
            $filesystem->dumpFile(
                sprintf('%s/%s.%s', $this->directory, $filename, $extension),
                $content
            );
        } catch (Throwable $e) {
            return false;
        }

        return true;
    }

    /**
     * @param string $filename
     * @param string $extension
     *
     * @return Files|null
     */
    private function getFiles(string $filename, string $extension): ?Files
    {
        if ($this->directory === null) {
            throw new RuntimeException('Before using this method, set the directory first.');
        }

        if (empty($filename)) {
            throw new LogicException('The filename cannot be empty.');
        }

        if (empty($extension)) {
            throw new LogicException('The extension cannot be empty.');
        }

        $files = (new Finder())->files();

        try {
            $files->in($this->directory);
        } catch (Throwable $e) {
            // should I create a directory?
            return null;
        }

        $files->name(sprintf('%s.%s', $filename, $extension));

        if (!$files->hasResults()) {
            return null;
        }

        $this->files = new Files();

        foreach ($files as $key => $file) {
            $this->files->add($file);
        }

        if ($this->files->isEmpty()) {
            return null;
        }

        return $this->files;
    }

    /**
     * @inheritDoc
     */
    public function setDirectory(string $directory = ''): void
    {
        $this->directory = sprintf('%s/%s', $this->filesDir, $directory);
    }

    /**
     * @inheritDoc
     */
    public function createTxtFile(string $filename): bool
    {
        return $this->saveToFile($filename, 'txt');
    }

    /**
     * @inheritDoc
     */
    public function saveToTxtFile(string $filename, string $content): bool
    {
        return $this->saveToFile($filename, 'txt', $content);
    }

    /**
     * @inheritDoc
     */
    public function getTxtFiles(string $filename): ?Files
    {
        return $this->getFiles($filename, 'txt');
    }

    /**
     * @inheritDoc
     */
    public function reset(): void
    {
        $this->directory = null;
        $this->files = null;
    }
}