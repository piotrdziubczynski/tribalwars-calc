<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 28.01.2020
 * Time: 21:51
 */

declare(strict_types=1);

namespace App\Service;

use Goutte\Client;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Interface FetchInterface
 *
 * @package App\Service
 */
interface FetchInterface
{
    /**
     * @return Client
     */
    public function getClient(): Client;

    /**
     * @return Crawler
     */
    public function getDom(): Crawler;

    /**
     * @param string $uri
     * @param array $parameters
     */
    public function get(string $uri, array $parameters = []): void;

    /**
     * @param string $uri
     * @param array $parameters
     */
    public function post(string $uri, array $parameters = []): void;
}