<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 03.02.2020
 * Time: 20:56
 */

declare(strict_types=1);

namespace App\Service\Manager;

use App\Service\FileInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * Class AbstractManager
 *
 * @package App\Service\Manager
 */
abstract class AbstractManager
{
    protected FileInterface $file;
    protected SerializerInterface $serializer;
    protected SessionInterface $session;
    protected SluggerInterface $slugger;

    /**
     * AbstractManager constructor.
     *
     * @param FileInterface $file
     * @param SerializerInterface $serializer
     * @param SessionInterface $session
     * @param SluggerInterface $slugger
     */
    public function __construct(
        FileInterface $file,
        SerializerInterface $serializer,
        SessionInterface $session,
        SluggerInterface $slugger
    )
    {
        $this->file = $file;
        $this->serializer = $serializer;
        $this->session = $session;
        $this->slugger = $slugger;
    }
}