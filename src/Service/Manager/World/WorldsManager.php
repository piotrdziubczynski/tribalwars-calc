<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 30.01.2020
 * Time: 22:44
 */

declare(strict_types=1);

namespace App\Service\Manager\World;

use App\Common\Collections\Worlds;
use App\Model\World;
use App\Model\World\Data;
use App\Model\WorldInterface;
use App\Service\CryptoInterface;
use App\Service\FetchInterface;
use App\Service\FileInterface;
use App\Service\Manager\AbstractManager;
use RuntimeException;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\DomCrawler\Link;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * Class WorldsManager
 *
 * @package App\Service\Manager\World
 */
final class WorldsManager extends AbstractManager
{
    private CryptoInterface $crypto;
    private FetchInterface $fetch;
    private ?Worlds $openedWorlds;
    private array $worlds;

    /**
     * WorldsManager constructor.
     *
     * @param CryptoInterface $crypto
     * @param FetchInterface $fetch
     * @param FileInterface $file
     * @param SerializerInterface $serializer
     * @param SessionInterface $session
     * @param SluggerInterface $slugger
     */
    public function __construct(
        CryptoInterface $crypto,
        FetchInterface $fetch,
        FileInterface $file,
        SerializerInterface $serializer,
        SessionInterface $session,
        SluggerInterface $slugger
    ) {
        parent::__construct($file, $serializer, $session, $slugger);

        $this->crypto = $crypto;
        $this->fetch = $fetch;
        $this->openedWorlds = null;
        $this->worlds = [];

        $this->fetch->get('https://pl.twstats.com/');
        $this->fetchWorlds();
    }

    /**
     * @param Crawler $world
     *
     * @return string
     */
    private function getWorldType(Crawler $world): string
    {
        preg_match(sprintf('/\((%s)\)/i', World::CLOSED), $world->text(), $matches);
        return mb_strtolower($matches[1] ?? World::OPENED);
    }

    /**
     * @param Link $link
     *
     * @return string
     */
    private function getWorldId(Link $link): string
    {
        preg_match('/\/(\w+)\/index\.php$/i', $link->getUri(), $matches);
        return mb_strtolower($matches[1] ?? '');
    }

    /**
     * @param string $string
     *
     * @return int
     */
    private function getNumberFromString(string $string): int
    {
        preg_match('/^(\d+)/i', preg_replace('/\W/i', '', $string), $matches);
        return (int)($matches[1] ?? 0);
    }

    /**
     * @param Crawler $worldData
     *
     * @return Data
     */
    private function getWorldData(Crawler $worldData): Data
    {
        [$players, $tribes, $villages] = explode(', ', $worldData->text());

        return new Data(
            $this->getNumberFromString($players),
            $this->getNumberFromString($tribes),
            $this->getNumberFromString($villages)
        );
    }

    /**
     * @param Crawler $world
     */
    private function createWorld(Crawler $world): void
    {
        $anchor = $world->children('a')->first();
        $span = $world->children('span')->first();

        $type = $this->getWorldType($world);
        $id = $this->getWorldId($anchor->link());
        $name = $anchor->text();
        $data = $this->getWorldData($span);

        if (!empty($id)) {
            $this->worlds[$type][$id] = new World($id, $name, $data);
        }
    }

    public function fetchWorlds(): void
    {
        $crawler = $this->fetch->getDom();
        $worlds = $crawler
            ->filter('#main > div:nth-of-type(3)')
            ->children('.widget')->last()
            ->filter('table.widget')
            ->children('tr')->first()
            ->filter('table.box');

        $worlds->filter('tr')
            ->each(
                function (Crawler $tr) {
                    $this->createWorld($tr->children('td')->last());
                }
            );
    }

    /**
     * @return array
     */
    public function getOpened(): array
    {
        if (empty($this->worlds)) {
            throw new RuntimeException('Before using this method, fetch the worlds first.');
        }

        $openWorlds = $this->worlds[World::OPENED] ?? [];

        if (empty($openWorlds)) {
            return [];
        }

        krsort($openWorlds);
        $this->openedWorlds = new Worlds(array_values($openWorlds));
        return $this->openedWorlds->getValues();
    }

    /**
     * @param string $id
     *
     * @return WorldInterface|null
     */
    public function getOpenedWorldById(string $id): ?WorldInterface
    {
        if ($this->openedWorlds === null) {
            $this->getOpened();
        }

        $openedWorlds = $this->openedWorlds->filter(
            function (WorldInterface $world) use ($id) {
                return $world->getId() === $id;
            }
        );

        if ($openedWorlds->isEmpty()) {
            return null;
        }

        return $openedWorlds->first();
    }
}