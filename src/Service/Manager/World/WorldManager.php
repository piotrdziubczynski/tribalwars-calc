<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 30.01.2020
 * Time: 22:44
 */

declare(strict_types=1);

namespace App\Service\Manager\World;

use App\Common\Collections\Units;
use App\DataTransferObject\ToWorldConfigDto;
use App\DataTransferObject\ToWorldUnitsDto;
use App\Model\Api\Units\Config as UnitsConfig;
use App\Model\Api\World\Config as WorldConfig;
use App\Model\Unit;
use App\Model\World\Config;
use App\Model\WorldInterface;
use App\Service\CryptoInterface;
use App\Service\FetchInterface;
use App\Service\FileInterface;
use App\Service\Manager\AbstractManager;
use RuntimeException;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Throwable;

/**
 * Class WorldManager
 *
 * @package App\Service\Manager\World
 */
final class WorldManager extends AbstractManager
{
    private ?Config $config;
    private CryptoInterface $crypto;
    private FetchInterface $fetch;
    private ?ToWorldConfigDto $toWorldConfig;
    private ?ToWorldUnitsDto $toWorldUnits;
    private ?Units $units;
    private ?WorldInterface $world;

    /**
     * WorldManager constructor.
     *
     * @param CryptoInterface $crypto
     * @param FetchInterface $fetch
     * @param FileInterface $file
     * @param SerializerInterface $serializer
     * @param SessionInterface $session
     * @param SluggerInterface $slugger
     */
    public function __construct(
        CryptoInterface $crypto,
        FetchInterface $fetch,
        FileInterface $file,
        SerializerInterface $serializer,
        SessionInterface $session,
        SluggerInterface $slugger
    ) {
        parent::__construct($file, $serializer, $session, $slugger);

        $this->config = null;
        $this->crypto = $crypto;
        $this->fetch = $fetch;
        $this->toWorldConfig = null;
        $this->toWorldUnits = null;
        $this->units = null;
        $this->world = null;
    }

    private function fetchConfig(): void
    {
        if ($this->world === null) {
            throw new RuntimeException('Before using this method, set the world first.');
        }

        $this->fetch->get(
            sprintf(
                'https://%s.plemiona.pl/interface.php?func=get_config',
                $this->world->getId()
            )
        );

        try {
            /** @var WorldConfig|null $apiConfig */
            $apiConfig = $this->serializer->deserialize(
                $this->fetch->getDom()->outerHtml(),
                WorldConfig::class,
                'xml'
            );
        } catch (Throwable $e) {
            return;
        }

        if ($apiConfig === null) {
            return;
        }

        $this->toWorldConfig = new ToWorldConfigDto($apiConfig);
        $this->config = $this->toWorldConfig->getApiConfig();

        $this->file->saveToTxtFile('config', $this->crypto->encode($this->config));
    }

    private function fetchUnits(): void
    {
        if ($this->world === null) {
            throw new RuntimeException('Before using this method, set the world first.');
        }

        $this->fetch->get(
            sprintf(
                'https://%s.plemiona.pl/interface.php?func=get_unit_info',
                $this->world->getId()
            )
        );

        try {
            /** @var UnitsConfig|null $apiConfig */
            $apiConfig = $this->serializer->deserialize(
                $this->fetch->getDom()->outerHtml(),
                UnitsConfig::class,
                'xml'
            );
        } catch (Throwable $e) {
            return;
        }

        if ($apiConfig === null) {
            return;
        }

        $this->toWorldUnits = new ToWorldUnitsDto($apiConfig);
        $this->toWorldUnits->setWorld($this->world);
        $this->units = $this->toWorldUnits->getUnits();

        $this->file->saveToTxtFile('units', $this->crypto->encode($this->units));
    }

    /**
     * @param array $units
     *
     * @return Units
     */
    private function convertToUnits(array $units): Units
    {
        array_walk(
            $units,
            function (&$unit) {
                $unit = $this->serializer->deserialize(
                    $this->serializer->serialize($unit, 'json'),
                    Unit::class,
                    'json'
                );
            }
        );

        return new Units($units);
    }

    /**
     * @param WorldInterface|null $world
     */
    public function setWorld(?WorldInterface $world): void
    {
        $this->world = $world;
    }

    public function setFilesDirectory(): void
    {
        if ($this->world === null) {
            throw new RuntimeException('Before using this method, set the world first.');
        }

        $this->file->setDirectory(sprintf('worlds/%s', $this->world->getId()));
    }

    /**
     * @return Config|null
     */
    public function getConfig(): ?Config
    {
        if ($this->world === null) {
            throw new RuntimeException('Before using this method, set the world first.');
        }

        $config = $this->file->getTxtFiles('config');

        if ($config !== null) {
            /** @var SplFileInfo $file */
            $file = $config->first();
            $this->config = $this->crypto->decode($file->getContents(), Config::class);
        }

        if ($this->config === null) {
            $this->fetchConfig();
        }

        return $this->config;
    }

    public function fillConfig(): void
    {
        if ($this->world === null) {
            throw new RuntimeException('Before using this method, set the world first.');
        }

        $this->world->setConfig($this->getConfig());
    }

    /**
     * @return Units|null
     */
    public function getUnits(): ?Units
    {
        if ($this->world === null) {
            throw new RuntimeException('Before using this method, set the world first.');
        }

        $units = $this->file->getTxtFiles('units');

        if ($units !== null) {
            /** @var SplFileInfo $file */
            $file = $units->first();
            $this->units = $this->convertToUnits($this->crypto->decode($file->getContents(), ''));
        }

        if ($this->units === null) {
            $this->fetchUnits();
        }

        return $this->units;
    }

    public function fillUnits(): void
    {
        if ($this->world === null) {
            throw new RuntimeException('Before using this method, set the world first.');
        }

        $this->world->setUnits($this->getUnits());
    }
}