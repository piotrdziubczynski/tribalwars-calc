<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 09.02.2020
 * Time: 23:04
 */

declare(strict_types=1);

namespace App\DataTransferObject;

use App\Common\Collections\Units;
use App\Model\Api\Units\Config as ApiConfig;
use App\Model\Unit;
use App\Model\UnitInterface;
use App\Model\WorldInterface;
use RuntimeException;

/**
 * Class ToWorldUnitsDto
 *
 * @package App\DataTransferObject
 */
final class ToWorldUnitsDto
{
    private ?ApiConfig $config;
    private ?WorldInterface $world;

    /**
     * ToWorldUnitsDto constructor.
     *
     * @param ApiConfig|null $config
     */
    public function __construct(?ApiConfig $config)
    {
        $this->config = $config;
        $this->world = null;
    }

    /**
     * @param ApiConfig\AbstractUnit|null $apiUnit
     *
     * @return UnitInterface|null
     */
    private function getUnit(?ApiConfig\AbstractUnit $apiUnit): ?UnitInterface
    {
        if ($apiUnit === null || $this->world === null) {
            return null;
        }

        $worldConfig = $this->world->getConfig();

        if ($worldConfig === null) {
            return null;
        }

        return new Unit(
            $apiUnit->getName(),
            new Unit\Speed((float)$apiUnit->getSpeed() / $worldConfig->getSpeed()->getWorld()),
            new Unit\Stats(
                new Unit\Stats\Attack(
                    $apiUnit->getAttack()
                ),
                new Unit\Stats\Defense(
                    $apiUnit->getDefense(),
                    $apiUnit->getDefenseCavalry(),
                    $apiUnit->getDefenseArcher()
                )
            )
        );
    }

    /**
     * @param ApiConfig|null $config
     */
    public function setApiConfig(?ApiConfig $config): void
    {
        $this->config = $config;
    }

    /**
     * @param WorldInterface|null $world
     */
    public function setWorld(?WorldInterface $world): void
    {
        $this->world = $world;
    }

    /**
     * @return Units
     */
    public function getUnits(): Units
    {
        if ($this->config === null) {
            throw new RuntimeException('Before using this method, set the API config first.');
        }

        if ($this->world === null) {
            throw new RuntimeException('Before using this method, set the world first.');
        }

        $units = new Units();
        $units->add($this->getUnit($this->config->getSpear()));
        $units->add($this->getUnit($this->config->getSword()));
        $units->add($this->getUnit($this->config->getAxe()));
        $units->add($this->getUnit($this->config->getArcher()));
        $units->add($this->getUnit($this->config->getSpy()));
        $units->add($this->getUnit($this->config->getLight()));
        $units->add($this->getUnit($this->config->getMarcher()));
        $units->add($this->getUnit($this->config->getHeavy()));
        $units->add($this->getUnit($this->config->getRam()));
        $units->add($this->getUnit($this->config->getCatapult()));
        $units->add($this->getUnit($this->config->getKnight()));
        $units->add($this->getUnit($this->config->getSnob()));

        return $units;
    }
}