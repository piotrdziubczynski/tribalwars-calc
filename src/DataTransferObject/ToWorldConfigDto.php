<?php
/**
 * Created by PhpStorm.
 * User: -n3veR
 * Date: 09.02.2020
 * Time: 23:04
 */

declare(strict_types=1);

namespace App\DataTransferObject;

use App\Model\Api\World\Config as ApiConfig;
use App\Model\World\Config;
use RuntimeException;

/**
 * Class ToWorldConfigDto
 *
 * @package App\DataTransferObject
 */
final class ToWorldConfigDto
{
    private ?ApiConfig $apiConfig;

    /**
     * ToWorldConfigDto constructor.
     *
     * @param ApiConfig|null $config
     */
    public function __construct(?ApiConfig $config)
    {
        $this->apiConfig = $config;
    }

    /**
     * @param ApiConfig|null $config
     */
    public function setApiConfig(?ApiConfig $config): void
    {
        $this->apiConfig = $config;
    }

    /**
     * @return Config
     */
    public function getApiConfig(): Config
    {
        if ($this->apiConfig === null) {
            throw new RuntimeException('Before using this method, set the API config first.');
        }

        return new Config(
            $this->apiConfig->getCommands()->getMillisArrival(),
            new Config\Speed(
                $this->apiConfig->getSpeed(),
                $this->apiConfig->getUnitSpeed()
            ),
            new Config\Units(
                $this->apiConfig->getGame()->getArcher(),
                $this->apiConfig->getGame()->getKnight()
            )
        );
    }
}